
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;


@WebServlet("/register")
public class RegistrationServlet extends HttpServlet {

        private static final long serialVersionUID = 1L;
        private PersonDao personDao;


        public void init() {
            personDao = new PersonDao();
        }

        protected void doPost(HttpServletRequest request, HttpServletResponse response)
                throws ServletException, IOException {

            String first_name = request.getParameter("first_name");
            String last_name = request.getParameter("last_name");
            String username = request.getParameter("username");
            String password = request.getParameter("password");
            String age = request.getParameter("age");
            String email = request.getParameter("email");

            Person people = new Person();
            people.setFirst_name(first_name);
            people.setLast_name(last_name);
            people.setUsername(username);
            people.setPassword(password);
            people.setAge(age);
            people.setEmail(email);

            try {
                personDao.registerPerson(people);
            } catch (Exception e) {

                e.printStackTrace();
            }

            response.sendRedirect("successfull.jsp");
        }
    }