import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class PersonDao {

    public int registerPerson(Person people) throws ClassNotFoundException {
        String INSERT_USERS_SQL = "INSERT INTO people" +
                "  ( first_name, last_name, username, password, age, email) VALUES " +
                " ( ?, ?, ?, ?,?,?);";


        int result = 0;

        Class.forName("com.mysql.jdbc.Driver");

        try (Connection connection = DriverManager
                .getConnection("jdbc:mysql://localhost:3306/Basa", "root", "Maga");

             PreparedStatement preparedStatement = connection.prepareStatement(INSERT_USERS_SQL)) {

            preparedStatement.setString(1, people.getFirst_name());
            preparedStatement.setString(2, people.getLast_name());
            preparedStatement.setString(3, people.getUsername());
            preparedStatement.setString(4, people.getPassword());
            preparedStatement.setString(5, people.getAge());
            preparedStatement.setString(6, people.getEmail());



            result = preparedStatement.executeUpdate();

        } catch (SQLException e) {

            printSQLException(e);
        }
        return result;
    }

    private void printSQLException(SQLException ex) {
        for (Throwable e: ex) {
            if (e instanceof SQLException) {
                e.printStackTrace(System.err);
                System.err.println("SQLState: " + ((SQLException) e).getSQLState());
                System.err.println("Error Code: " + ((SQLException) e).getErrorCode());
                System.err.println("Message: " + e.getMessage());
                Throwable t = ex.getCause();
                while (t != null) {
                    System.out.println("Cause: " + t);
                    t = t.getCause();
                }
            }
        }
    }
}