<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>Login Application</title>
</head>

<body >
<form action="LoginServlet" method="post">
    <center>
    <fieldset style="width: 300px">
        <legend> Login to App </legend>
        <table>
            <tr>
                <td>User ID</td>
                <td><input type="text" name="username" required="required" /></td>
            </tr>
            <tr>
                <td>Password</td>
                <td><input type="password" name="password" required="required" /></td>
            </tr>
            <tr>
                <td><input type="submit" value="Login" /></td>
            </tr>
            <tr>
                <td colspan="2">New User <a href="register.jsp">Register Here</a></td>
            </tr>
        </table>
    </fieldset>
        </center>
</form>

</body>
</html>
