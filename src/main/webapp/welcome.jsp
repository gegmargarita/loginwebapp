<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>Welcome <%=session.getAttribute("name")%></title>
    <style>
        h2 {
            color: Red;
            display: block;
            font-size: 2em;
            margin-top: 0.67em;
            margin-bottom: 0.67em;
            margin-left: 0;
            margin-right: 0;
            font-weight: bold;
        }
        h3 {
            color: black;
            display: block;
            font-size: 1em;
            margin-top: 0.67em;
            margin-bottom: 0.67em;
            margin-left: 0;
            margin-right: 0;
            font-weight: bold;
        }
    </style>
</head>

</head>
<body>
<center>
<h2>Login successful!!!</h2>
<h3>
    Hello,
    <%=session.getAttribute("name")%></h3>
</center>
</body>
</html>