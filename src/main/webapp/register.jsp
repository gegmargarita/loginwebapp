<<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
          pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>Register Page</title>
</head>
<body>
<center>
<h1>Registration Form</h1>
<div class="ex">
    <form action="RegistrationServlet" method="post">
        <table >
            <tr>
                <td>First Name</td>
                <td><input type="text" name="first_name" required="required" /></td>
            </tr>
            <tr>
                <td>Last Name</td>
                <td><input type="text" name="last_name" required="required" /></td>
            </tr>
            <tr>
                <td>User Name</td>
                <td><input type="text" name="username" required="required" /></td>
            </tr>
            <tr>
                <td>Password</td>
                <td><input type="password" name="password" required="required" /></td>
            </tr>
            <tr>
                <td>Age</td>
                <td><input type="text" name="age" required="required" /></td>
            </tr>
            <tr>
                <td>Email </td>
                <td><input type="text" name="email"required="required" /> </td>
            </tr>



        </table>
        <input type="submit" value="register" />
    </form>
</div>
</center>
</body>
</html>